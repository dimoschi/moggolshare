from django import views
from django.shortcuts import render, redirect
from .forms import FilesForm


class FilesUploadView(views.View):
    template = 'files/model_form_upload.html'
    form = FilesForm()

    def get(self, request):
        return render(request, self.template, {
            'form': self.form
        })

    def post(self, request):
        if request.method == 'POST':
            form = FilesForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                return redirect('success')
        else:
            form = FilesForm()
        return render(request, self.template, {
            'form': form
        })
