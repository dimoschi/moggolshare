from django.conf.urls import url
from django.views.generic import TemplateView
from files import views

urlpatterns = [
    url(r'^file_upload$', views.FilesUploadView.as_view(), name='file_upload'),
    url(r'^success$', TemplateView.as_view(
        template_name="files/success.html"), name='success'
    )
]
